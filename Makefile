# makefile for fqme
#
# SPDX-FileCopyrightText: (C) Eric S. Raymond <esr@thyrsus.com>
# SPDX-License-Identifier: BSD-2-Clause

VERS=$(shell sed -n <NEWS.adoc '/^v[0-9]/s/:.*//p' | head -1)

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

.PHONY: build fmt lint

build:
	go build demo/fqme-demo.go

lint:
	golint .
	go vet .

reflow:
	gofmt -w .

check: lint build

SOURCES = README.adoc LICENSE NEWS.adoc control Makefile fqme.go demo/fqme-demo.go smalleye.png

version:
	@echo $(VERS)

fqme-$(VERS).tar.gz: $(SOURCES)
	@ls $(SOURCES) | sed s:^:fqme-$(VERS)/: >MANIFEST
	@(cd ..; ln -s fqme fqme-$(VERS))
	(cd ..; tar -czvf fqme/fqme-$(VERS).tar.gz `cat fqme/MANIFEST`)
	@(cd ..; rm fqme-$(VERS))

dist: fqme-$(VERS).tar.gz

release: fqme-$(VERS).tar.gz
	shipper version=$(VERS) | sh -e -x

refresh: fqme.html
	shipper -N -w version=$(VERS) | sh -e -x
