package main

import (
	"fmt"
	"gitlab.com/esr/fqme"
)

func main() {
	name, email, _ := fqme.WhoAmI()
	fmt.Printf("name: %s\nemail: %s\n", name, email)
}
